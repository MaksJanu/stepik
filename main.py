import pytest

def bubble_sort(arr):
    for _ in range(len(arr) - 1):
        for i in range(len(arr) - 1):
            if arr[i] > arr[i + 1]:
                arr[i], arr[i + 1] = arr[i + 1], arr[i]
    return arr


def test_bubble_sort():
    nums = [8,3,0,1]
    assert bubble_sort(nums) == sorted(nums)
